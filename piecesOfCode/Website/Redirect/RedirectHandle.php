<?php

namespace Kei\WebsiteBundle\Tools\Redirect;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * klasa wyszukuje w bazie przekierowan url, ktory ma zostac przekierowany
 * Class RedirectHandle
 * @package Kei\WebsiteBundle\Tools\Redirect
 */
class RedirectHandle
{
    /**
     * @var string
     */
    private $kernelEnvironment;
    /**
     * @var string
     */
    private $container;
    /**
     * @var string
     */
    private $redirectUrl;
    
    const DEVELOPER_ENV = "dev";
    /**
     * RedirectHandle constructor.
     * @param ContainerInterface $containerInterface
     */
    public function __construct(ContainerInterface $containerInterface)
    {
        $this->container = $containerInterface;
        $this->kernelEnvironment = $this->container->get('kernel')->getEnvironment();
    }

    /**
     * Pobiera uri strony i jesli posiada przekierowanie w bazie to tworzy request 301
     */
    public function handleUrl()
    {
        if ($this->kernelEnvironment === self::DEVELOPER_ENV) {
            $em = $this->container->get("doctrine.orm.entity_manager");
            $repository = $em->getRepository("KeiWebsiteBundle:ModuleRedirect");
            $url =  $repository->findByUrlFromRedirect($this->cutUri());
            if ($url && !empty($this->cutUri())) {
                $this->redirectUrl = $url[0]->getUrlToRedirect();
                if ((string)$url[0]->getUrlFromRedirect() === (string)$this->cutUri()) {
                    return $this->redirectingUrl();
                }
            }
            return false;
        }
    }

    /**
     * Obcina request uri ze wzgledu na app_dev.php
     */
    private function cutUri()
    {
        return substr($_SERVER['REQUEST_URI'], 12);
    }

    /**
     * Przekierowuje strone
     */
    private function redirectingUrl()
    {
        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $this->redirectUrl)) {
            $this->redirectUrl = 'https://' . $_SERVER["SERVER_NAME"] . '/app_dev.php' . $this->redirectUrl;
        }
        return $this->redirectUrl;
    }
}


