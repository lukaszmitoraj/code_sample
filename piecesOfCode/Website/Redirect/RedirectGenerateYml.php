<?php

namespace Kei\WebsiteBundle\Tools\Redirect;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Klasa tworzy plik yml z wygenerowanymi przekierowaniami
 * Class RedirectHandle
 * @package Kei\WebsiteBundle\Tools\Redirect
 */
class RedirectGenerateYml
{
    public $infoMessage;
    const NAME_REDIRECT = "redirect_301";
    /**
     * @var string
     */
    private $container;
    /**
     * Tablica zawiera wszystkie przekierowania, ktore beda zapisane do yml-a
     * @var array
     */
    private $arrayToYml = array();
    /**
     * RedirectGenerateYml constructor.
     * @param ContainerInterface $containerInterface
     */
    public function __construct(ContainerInterface $containerInterface)
    {
        $this->container = $containerInterface;
    }
    /**
     * Pobiera wszystkie przekierowania
     */
    private function getAllRedirects()
    {
        $em = $this->container->get("doctrine.orm.entity_manager");
        $repository = $em->getRepository("KeiWebsiteBundle:ModuleRedirect");
        $redirects = $repository->findAll();
        if (empty($redirects)) {
            throw new \Exception("Nie znaleziono żadnych przekierowań");
        }
        return $redirects;
    }
    /**
     * Dodaje przekierowania do tablicy, której index ma postać:
     * { $nameRouting=>array("path"=>link_do_przekierowania,'defaults'=>array(
     *                       "path"=>"link_do_ktorego_nastapi_przekierowania",
     *                       "_controller"=>"kontroller odpowiedzialny za wykonanie przekierowania"
     *                       "permanent" => "ustawienie permanentosci na true/false, czyli 301"
     *                      ))
     * }
     *
     */
    private function addRedirectToArray()
    {
        foreach ($this->getAllRedirects() as $redirect) {
            $nameRouting = $this->generateNameFromRedirectUrl($redirect);
            $this->arrayToYml[$nameRouting]["path"] = str_replace('&amp;','&',htmlspecialchars($redirect->getUrlFromRedirect()));
            $this->arrayToYml[$nameRouting]["defaults"]
                             ["path"] = $redirect->getUrlToRedirect();
            $this->arrayToYml[$nameRouting]["defaults"]["permanent"] = true;
            $this->arrayToYml[$nameRouting]["defaults"]["_controller"] = 'FrameworkBundle:Redirect:urlRedirect';
        }
    }

    /**
     * Generuje nazwe klucza do tablicy poprzez usuniecie wybranych znakow i zastapienie ich podkreslnikami
     * @param $stringToGenerateName
     * @return string
     * @throws \Exception
     */
    private function generateNameFromRedirectUrl($stringToGenerateName)
    {
      $stringFromGenerateName = $stringToGenerateName->getUrlFromRedirect();
      if (strlen($stringFromGenerateName) === 0) {
          throw new \Exception("Nie można wygenerować nazwy. Długość napisu: $stringToGenerateName jest równa zeru.");
      }
      if (strlen( $stringFromGenerateName) === 1) {
            return self::NAME_REDIRECT.$stringToGenerateName->getId();
      }
      return self::NAME_REDIRECT.str_replace(array('/','.','-',':'),'_', $stringFromGenerateName);
    }

    /**
     * Generuje i zapisuje plik Yml z tablicy w której znajdują przekierowania
     * @throws \Exception
     */
    private function generateYmlFile()
    {
      $dumpYml =  Yaml::dump($this->arrayToYml,10);
      if ( !$dumpYml) {
          throw new \Exception("Wygenerowanie danych do yml nie powiodło się. Sprawdź, czy podałeś poprawne dane.");
      }
      $fileYmlWithRedirects = $this->container->getParameter("redirect_301_yml_file");
      chmod($fileYmlWithRedirects,0664);
      if(!file_put_contents($fileYmlWithRedirects,$dumpYml)) {
          throw new \Exception("Zapisanie do pliku nie powiodło się. 
                                Sprawdź, czy plik istnieje i/lub posiadasz odpowiednie uprawnienia"
                              );
      }

      return $this->infoMessage =  "Zapis do pliku $fileYmlWithRedirects powiódł się pomyślnie";
    }
    /**
     * Uruchamia generowanie yml
     */
    public function init()
    {
        $this->addRedirectToArray();
        return $this->generateYmlFile();
    }
}
