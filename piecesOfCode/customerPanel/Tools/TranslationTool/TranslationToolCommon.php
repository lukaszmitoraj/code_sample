<?php
/**
 * Created by PhpStorm.
 * User: lukasz
 * Date: 03.08.16
 * Time: 12:37
 */
namespace Kei\CustomerPanelBundle\Tools\TranslationTool;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Kei\CustomerPanelBundle\Tools\TranslationTool\Exceptions\TranslToolException;
use Kei\CustomerPanelBundle\Tools\TranslationTool\Settings\TranslSettings;

/**
 * Class TranslationToolCommon
 * @package Kei\CustomerPanelBundle\Tools\TranslationTool
 * @SuppressWarnings
 */
abstract class TranslationToolCommon
{
    const VENDOR_DIR = "vendor";
    /**
     * Komenda w ustawieniach, dzieki ktorej dla akcji pobierane sa wszystkie bundle
     * Ex. php app/console translate:generate all
     */
    const USE_ALL_BUNDLES = "all";
    protected static $possiblePathInVendor = array("/src/Kei", "/Kei");
    protected static $ignoreFiles = array('backup'=>1);
    public static $availableLanguages = ['en', 'pl'];
    public static $languageDefaultFilter = array('en');
    protected $keysTranslation = array();
    protected $newTranslations = array();
    protected $containerInterface;
    /**
     * Plik przechowuje aktualne tłumaczenia i w przypadku importu pliki trafiają wyłącznie tutaj.
     * Generowanie YML-ów odbywa się również z tego pliku.
     */
    protected $arrayFileToLanguage = array();
    /**
     * @var string
     */
    protected $translateSettings;
    /**
     * @var string
     */
    protected $extraBehSettings;
    public $commandName = 'translate:generate';

    abstract public function run();

    /**
     * TranslationToolCommon constructor.
     * @param string $translateSettings ex:
     * 1.`generate_yml` bundle*:pathToSave**
     *    (*) - nazwa lub sciezka do bundle'a/vendora / zaleca się używanie jednak sciezki ex. src/Kei/CustomerPanelBundle
     *    (**) - sciezka, gdzie maja byc zapisane wygenerowane pliki yml - w przypadku brak ustawienia zostanie on wygenerowany do standardowej sciezki
     *    symfony ex. src/Kei/CustomerPanelBundle
     * 2. `import` pathFileImport*:bundleToImport**:pathToSave***
     *     (*) - sciezka do pliku xls z nowymi tlumaczeniami
     *     (**) - nazwa bundle'a z ktorego maja zostac pobrane nowe tlumaczenia. W przypadku braku ustawien, zostana zaimportowane wszystkie
     *     (***) - sciezka do bundle'a do ktorego maja zostac dograne nowe tlumaczenia. W przypadku braku ustawienia, zostanie ona pobrana z pliku xls.
     *             Ex. src/Kei/CustomerPanelBundle
     * @param ContainerInterface $containerInterface
     * @param string $actionType - import/export/generate_yml
     * @param string $extraBehSettings - dodatkowe ustawienia zachowania dla tlumaczen akcji np. changedkeys/newkeys/update_yml
     */
    public function __construct($translateSettings, $extraBehSettings, ContainerInterface $containerInterface, $actionType)
    {
        $this->translateSettings =  new TranslSettings($translateSettings, $actionType);
        $this->containerInterface = $containerInterface;
        $this->extraBehSettings = $extraBehSettings;
    }


    /**
     * Sprawdza czy plik istnieje na podstawie sciezki
     * @param $file
     * @throws TranslToolException
     */
    protected function checkFileIsExists($file)
    {
        if (!file_exists($file)) {
            throw new TranslToolException("Błąd pliku: $file. Sprawdź, czy posiadasz odpowiednie uprawnienia
            lub, czy folder istnieje");
        }
    }
    /**
     * Zapisuje do pliku Json
     * @param array $dateToJson
     * @param string $fileToSave - sciezka do zapisu pliku json
     * @throws TranslToolException
     */
    protected function generateJSON($dateToJson, $fileToSave)
    {
        if (empty($dateToJson)) {
            throw new TranslToolException("Niestety nie posiadamy obecnie żadnych danych do zapisu.");
        }
        ksort($dateToJson);
        $fileJson = json_encode($dateToJson, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        $this->saveData($fileToSave, $fileJson);
    }

    /**
     * Zapisuje dane do pliku z obsluga wlasnego wyjatku
     * @param $fileToSave
     * @param $data
     * @throws TranslToolException
     */
    protected function saveData($fileToSave, $data)
    {
        if (!@file_put_contents($fileToSave, $data)) {
            throw new TranslToolException("Nie można zapisać danych do pliku $fileToSave");
        }
    }

    /**
     * Sprawdza, czy plik jest odpowiedniego rozszerzenia
     * @param array $extension ex('xls,xlsx')
     * @throws TranslToolException
     * @param string $fileToCheck
     */
    protected function checkExtensionIsTrue($fileToCheck, $extension = array())
    {
        $fileInfo = pathinfo($fileToCheck);
        if (!in_array($fileInfo['extension'], $extension) || !isset($fileInfo)) {
            throw new TranslToolException("Nie prawidłowe rozszerzenie pliku ".$fileInfo['extension']);
        }
    }

    /**
     * Funkcja przechodzi przez wybrane katalogi
     * @param $dir - sciezka do katalogu z plikami
     * @param bool $returnFiles - parametr opcjonalny, po ustawienia parametru na false, z $dir zwraca tylko foldery i sciezki do niej jako wartosci. Patrz nizej:D
     * @param string $onlyExtension
     * @return array with dirs
     */
    protected function handleDirectory($dir, $returnFiles = true, $onlyExtension = '*')
    {
        $filesArray = array();
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while ($file = readdir($dh)) {
                    $filePath = $dir.'/'.$file;
                    if ($file !== '.' && $file !=='..' &&
                        ($returnFiles ? !is_dir($filePath) : is_dir($filePath)) &&
                        !isset(self::$ignoreFiles[$file])
                    ) {
                        $filesArray = array_merge_recursive($filesArray, $this->fileDetails($filePath, $returnFiles, $onlyExtension));
                        // Ustawiamy sciezke dla folderu, jako wartosc jego klucza. Na przyklad "CsrBundle" => "nie/pamietam/sciezki"
                        if (!$returnFiles) {
                            $filesArray[$file] = $filePath;
                        }
                    }
                }
                closedir($dh);
            }
        }
        return $filesArray;
    }

    /**
     * Funkcja pobiera parametr uzywajac interfejsu symfony
     * @param string $param -parametr do pobrania
     * @return mixed
     * @throws TranslToolException
     */
    protected function getParamFromConfig($param)
    {
        $paramFromSet = $this->containerInterface->getParameter($param);
        if (!$paramFromSet) {
            throw new TranslToolException("Nie znaleziono paramatetru $param");
        }
        return $paramFromSet;
    }

    /**
     * Zwraca ostatni błąd dekodowania pliku JSON
     * @return bool
     * @throws TranslToolException
     */
    protected function hasJsonDecodeError()
    {
        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                throw new TranslToolException('Przekroczono maksymalny poziom zagnieżdżania danych');
                break;
            case JSON_ERROR_CTRL_CHAR:
                throw new TranslToolException('Znaleziono niespodziewany znak kontrolny');
                break;
            case JSON_ERROR_SYNTAX:
                throw new TranslToolException('Błąd składni. Źle skonstruowany JSON');
                break;
            case JSON_ERROR_NONE:
                return true;
                break;
        }
    }

    /**
     * Zwraca informacje o katalogu, badz pliku yml
     * W przypadku pliku zwraca tablice w postaci: "nazwaPliku" => array("jezyk pliku" => "path do pliku").
     * Jesli chcemy zas dostac informacje o folderze to zwraca tablice w postaci: "nazwaFolderu" => "path do folderu"
     * @param string $filePath
     * @param $isFile
     * @param string $onlyExtension
     * @return mixed
     */
    private function fileDetails($filePath, $isFile = true, $onlyExtension = '*')
    {
        $fileName = basename($filePath);
        $filesArray = [];
        $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
        $extensionIsAccepted = $onlyExtension !== '*' ? ($fileExtension === $onlyExtension): true;
        if ($isFile) {
            if ($extensionIsAccepted) {
                $fileExplode = explode('.', $fileName);
                $filesArray[$fileExplode[0]][$fileExplode[1]] = $filePath;
            }
            return $filesArray;
        }
        $filesArray[$fileName] = [$filePath];
        return $filesArray;
    }

    /**
     * Metoda, ktora uzupelnia zwracanie plikow/folderow tlumaczen.
     * Dodana ponieważ vendory posiadają różną strukture folderow, w ktorym sa umieszczone foldery do tlumaczen
     * @param string $dir
     * @param bool $isFile
     * @param string $onlyExtension
     * @return array|bool
     */
    protected function handleDirectoryVendor($dir, $isFile = false, $onlyExtension = "*")
    {
        $dirTranslate = "";
        foreach (self::$possiblePathInVendor as $possibleSource) {
            $checkDir = $dir.$possibleSource;
            if (is_dir($checkDir)) {
                $dirTranslate = $checkDir;
            }
        }
        if ($dirTranslate === "") {
            return false;
        }
        return $this->handleDirectory($dirTranslate, $isFile, $onlyExtension);
    }

    /**
     * Zwraca sciezki wszystkich folderow w ktorych sa tlumaczenia
     * @return array
     * @throws TranslToolException
     */
    protected function allTranslationDirs()
    {
        $translationDirs = [];
        $dirHandle = $this->getParamFromConfig('translate_tool.sourcesTranslate');
        foreach ($dirHandle as $pathDir) {
            echo $pathDir."\n";
            $translationDirs = array_merge($this->handleDirectory($pathDir, false), $translationDirs);
        }
        return $translationDirs;
    }

    /**
     * Sprawdza, czy podana przez uzytkownika sciezka istnieje. Jesli tak, to jest zwracana.
     * @param string $pathToCheck
     * @return null|string
     * @throws TranslToolException
     */
    protected function isSourcePathExist($pathToCheck)
    {
        // Sprawdzam czy uzytkownik podal od razu poprawna sciezke
        if (is_dir($pathToCheck)) {
            return $pathToCheck;
        }
        // Jesli wyzej nie jest poprawnie to spodziewam sie w takim wypadku nazwy bundle.
        $bundlePath = $this->getParamFromConfig("translate_tool.sourcesTranslate")["bundles"]."/".$pathToCheck;
        if (is_dir($bundlePath)) {
            return $bundlePath;
        }
        throw new TranslToolException("Ścieżka: $pathToCheck nie istnieje");
    }

    /**
     * Zwraca bundle dla ktorych maja zostac wygenerowane/pobrane tlumaczenia.
     * Jesli uzytkownik podal poprawna sciezke w ustawieniach to jest ona zwracana.
     * @return array
     */
    protected function getBundlesTranslate()
    {
        $bundles = [];
        $sourcePath = $this->translateSettings->getSourcePath();
        $sourcePath = $sourcePath && $sourcePath !== self::USE_ALL_BUNDLES ? $this->isSourcePathExist($sourcePath) : null;
        if (!$sourcePath) {
            $bundles = $this->allTranslationDirs();
        } else {
            $bundles[] = $sourcePath;
        }
        return $bundles;
    }
}
