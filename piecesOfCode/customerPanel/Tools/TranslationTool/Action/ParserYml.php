<?php
/***
 * Obslugiwana jest przez klase TranslationToolCommand
 * Klasa generuje yml z pliku JSON do wybranego lub domyslnego folderu
 */

namespace Kei\CustomerPanelBundle\Tools\TranslationTool\Action;

use Kei\CustomerPanelBundle\Tools\TranslationTool\TranslationToolCommon;
use Symfony\Component\Yaml\Yaml;
use Kei\CustomerPanelBundle\Tools\TranslationTool\Exceptions\TranslToolException;

/**
 * Created by PhpStorm.
 * User: lukasz
 * Date: 03.08.16
 * Time: 12:47
 */
class ParserYml extends TranslationToolCommon
{
    private $translatePath;
    public $translates = array();

    /**
     * Generowanie plikow YML do odpowiedniego folderu
     * @return string
     * @throws TranslToolException
     */
    public function run()
    {
        $translatesToGenerate = $this->getBundlesTranslate();
        if (empty($translatesToGenerate)) {
            throw new TranslToolException("Niestety nie można wygenerować tłumaczeń. Podany folder jest pusty");
        }
        return $this->generateYmlFile($translatesToGenerate);
    }

    /**
     * Funkcja pomocnicza do tworzenia tablicy dla tlumaczen z ktorego pozniej beda generowane pliki yml - $translates.
     * Ex. return: $this->translates["messages.en"]["msg"]["service"]["notfound"] = "cos tam"
     * @param string $ymlName - nazwa pliku yml ex. messages.en
     * @param array $translateLabels - tablica zagniezdzen dla tłumaczonych etykiet ex.array(msg, service, notfound)
     * @param string $translatedText - przetlumaczony tekst ex."cos tam"
     */
    private function createIndexArray($ymlName, $translateLabels, $translatedText)
    {
        $indexTranslate = "\$this->translates[\"$ymlName\"]";

        if (!empty($translateLabels)) {
            foreach ($translateLabels as $label) {
                $indexTranslate .= "[\"$label\"]";
            }
        }

        if ($translatedText) {
            // Robimy ucieczke, dla podwojnego cudzyslowia, a pozniej zamieniamy \/ na /, bo inaczej parser symfony dla yml zrobi z tego ucieczke.
            $translatedText = addcslashes($translatedText, '"\\/');
            $translatedText = str_replace('\/', '/', $translatedText);
        }
        $indexTranslate .= "=\"$translatedText\";";
        @eval($indexTranslate);
    }

    /**
     * Funkcja tworzy odpowiednia tablice potrzebna do Generowania YML
     * @param array $translates - tablica z której są generowane zmienne do yml
     * @param array $langKeys - tablica z językami
     * @throws TranslToolException
     */
    public function getTranslates($translates, $langKeys)
    {
        if (!is_array($langKeys) || empty($langKeys)) {
            throw new TranslToolException("Języki muszą być podane w tablicy i nie może być ona pusta.");
        }

        // Petla przechodzi po wszystkich dostepnych jezykach
        foreach ($langKeys as $langKey) {
            // Petla przechodzi po tlumaczeniach:
            // array('messages;onelement' => ["pl"=> "cos", "en" => "cos"],
            // 'messages;msg/service/notfound' => ["pl"=> "cos tam", "en" => "cos tam cos tam"],
            // 'messages;msg/service/found' => ["pl"=> "cos tam2", "en" => "cos tam cos tam2"])
            foreach ($translates as $label => $translateText) {
                // Rozbijamy $label na nazwe pliku oraz zagniezdzenie tlumaczenia: messages oraz onelement'
                $labelDetails = explode(';', $label);
                // Rozbijamy zagniezdzenie tlumaczenia na array(msg, onelement)
                $labels = $labelDetails[1];
                $ymlName = $labelDetails[0];
                $translateLabels = explode('/', $labels);
                // Jesli zagniedzenie ma wiecej niz jeden element uzywamy metody createIndexArray.
                // W przeciwnym wypadku dodajemy go do tablicy z ktorej beda generowane pozniej pliki yml:
                // 'messages.$langKey' => array('onelement' => '$translateText[$langKey]')
                $ymlFile = $ymlName.".$langKey";
                if (count($translateLabels) > 1) {
                    if (isset($translateText[$langKey])) {
                        $this->createIndexArray($ymlFile, $translateLabels, $translateText[$langKey]);
                    }
                } else {
                    if (isset($translateText[$langKey])) {
                        $this->translates[$ymlFile][$labels] = $translateText[$langKey];
                    }
                }
            }
        }
    }

    /**
     * Generowanie YML z tablicy
     * @param array $translationPaths - zawiera tablice w ktorej znajduja sie sciezki z ktorych beda generowane nowe tlumczenia
     * @throws TranslToolException
     * @return string
     */
    private function generateYmlFile($translationPaths)
    {
        foreach ($translationPaths as $path) {
            // Sprawdzamy, czy sciezka nie prowadzi do vendora kei, poniewaz wtedy sciezka do tlumaczen jest inna.
            if (preg_match("#".parent::VENDOR_DIR."#", $path)) {
                if ($vendorTranslate = $this->handleDirectoryVendor($path, false)) {
                    $path = reset($vendorTranslate);
                }
            }
            $this->translatePath = $path.$this->getParamFromConfig("translate_tool.fillSourceTranslate");
            $translateFile = $this->translatePath."/".$this->getParamFromConfig('translate_tool.json_file');
            if (file_exists($translateFile)) {
                $file = @file_get_contents($translateFile);
                $translateFileDecode = json_decode($file, true);
                $this->hasJsonDecodeError();
                $this->getTranslates($translateFileDecode, $this->translateSettings->getFilterLanguage());
                $this->saveYaml($this->translates, $this->getTransFilePath());
            }
        }

        return "Wygenerowanie plików YML przebiegło pomyślnie";
    }

    /**
     * @param array $dataToSave
     * @param string $pathSave
     * @return bool
     * @throws TranslToolException
     */
    public function saveYaml($dataToSave, $pathSave)
    {
        if (empty($dataToSave)) {
            return false;
        };
        foreach ($dataToSave as $file => $values) {
            $this->translates = [];
            $yaml = Yaml::dump($values, 10);
            $filePath = "$pathSave/$file.yml";
            $this->saveData($filePath, $yaml);
            $this->checkFileIsExists($filePath);
        }
    }

    /**
     * Zwraca sciezke do zapisania tlumaczen
     * @return bool|mixed|string
     */
    private function getTransFilePath()
    {
        $savePath = $this->translateSettings->getTargetPath();
        if (!$savePath) {
            $savePath = $this->translatePath;
        }
        $this->checkFileIsExists($savePath);
        return $savePath;
    }
}
