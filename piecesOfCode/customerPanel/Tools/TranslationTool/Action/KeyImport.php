<?php
/***
 * Obslugiwana jest przez klase TranslationToolCommand
 * Klasa generuje import kluczy z pliku xls
 */

namespace Kei\CustomerPanelBundle\Tools\TranslationTool\Action;

use Kei\CustomerPanelBundle\Tools\TranslationTool\TranslationToolCommon;
use PHPExcel_IOFactory;
use Kei\CustomerPanelBundle\Tools\TranslationTool\Exceptions\TranslToolException;
use Kei\CustomerPanelBundle\Tools\TranslationTool\Action\ParserYml;
use Symfony\Component\Yaml\Yaml;

class KeyImport extends TranslationToolCommon
{

    const UPDATE_YML = 'update_yml';
    const ENGLISH_LANG = 'en';
    const POLISH_LANG = 'pl';
    private $translatePath = "";

    /**
     * Glowna metoda do aktualizacji tlumaczen
     * @return string
     * @throws TranslToolException
     * @throws \PHPExcel_Reader_Exception
     */
    public function run()
    {
        $newTranslations = array();
        $importFile = $this->getImportFile();
        $this->checkFileIsExists($importFile);
        $this->checkExtensionIsTrue($importFile, array('xls','xlsx'));
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel = $objReader->load($importFile);

        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $newTranslations = $worksheet->toArray();
        }
        if (empty($newTranslations)) {
            throw new TranslToolException("Niestety plik:$importFile nie posiada żadnych tłumaczeń.");
        }
        $this->updateTranslations($this->getTranslations($newTranslations));
        return "Wygenerowanie do indeksu z pliku:".$importFile." przebiegło pomyślnie";
    }

    /**
     * Zwraca ustawiony przez uzytkownika plik xls z ktorego maja zostac zaimportowane tlumaczenia
     * @return bool|mixed
     */
    private function getImportFile()
    {
        return $this->translateSettings->getSourcePath();
    }

    /**
     * Aktualizuje tlumaczenia
     * @param $newTranslations
     * @throws TranslToolException
     */
    private function updateTranslations($newTranslations)
    {
        if (empty($newTranslations)) {
            throw new TranslToolException("Nie posiadamy żadnych nowych tłumaczeń do zapisu. ");
        }
        foreach ($newTranslations as $pathBundle => $translation) {
            $this->translatePath = $this->getParamFromConfig('translate_tool.kernel_dir'). $pathBundle;
            if ($this->extraBehSettings == self::UPDATE_YML) {
                $this->updateYmlFiles($translation);
            } else {
                $this->updatedJsonFiles($translation);
            }
        }
    }

    /**
     * Aktualizuje plik json z tlumaczeniami
     * @param array $importedTranslates
     * @throws TranslToolException
     */
    private function updatedJsonFiles($importedTranslates)
    {
        $translatesFile = $this->getPathTranslate();
        if (!($oldTranslations = @file_get_contents($translatesFile))) {
            throw new TranslToolException("Plik ".$translatesFile." nie został odnaleziony.");
        }
        $oldTranslations = json_decode($oldTranslations, true);
        $this->hasJsonDecodeError();
        $updatedTranslations = array_replace_recursive($oldTranslations, $importedTranslates);
        $this->generateJSON($updatedTranslations, $translatesFile);
    }
    /**
     * Aktualizuje yml-e z tlumaczeniami
     * @param array $importedTranslates
     * @throws TranslToolException
     */
    private function updateYmlFiles($importedTranslates)
    {
        $newTranslates = [];
        $parserYml = new ParserYml($this->translateSettings->getSettingString(), $this->extraBehSettings, $this->containerInterface, 'import');
        $parserYml->translates = [];
        $parserYml->getTranslates($importedTranslates, $this->getFilterLanguage());
        foreach ($parserYml->translates as $file => $translates) {
            $sourceYaml = $this->translatePath."/".$file.".yml";
            $newTranslates[$file] = array_replace_recursive(Yaml::parse($sourceYaml, false, true), $translates);
        }
        $parserYml->saveYaml($newTranslates, $this->translatePath);
    }
    /**
     * Zwraca sciezke do pliku json z tlumaczeniami do ktorego zostana zaimportowane tlumaczenia
     * @param bool $isJsonChange
     * @return bool|mixed|string
     */
    private function getPathTranslate($isJsonChange = true)
    {
        $file = "";
        if ($isJsonChange) {
            $file = $this->getParamFromConfig('translate_tool.json_file');
        }
        $savePath = $this->translateSettings->getTargetPath();
        if (!$savePath) {
            $savePath = $this->translatePath;
        }
        $this->checkFileIsExists($savePath);
        return $savePath."/".$file;
    }

    /**
     * Zwraca tłumaczenia, które mają zostać zaktualizowane.
     * @param array $translates
     * @throws TranslToolException
     * @return array
     */
    private function getTranslations($translates)
    {
        $translatesToImport = [];
        foreach ($translates as $translateDetails) {
            $bundle = $translateDetails[0];
            $keyTranslation = $translateDetails[1];
            $polishText = $translateDetails[2];
            $englishText = $translateDetails[3];
            // Sprawdzam, czy $bundle pasuje do filtru ustawionego przez uzytkownika
            if (preg_match("#".$this->getBundleFilter()."#", $bundle)) {
                $newTranslates = [self::POLISH_LANG => $polishText, self::ENGLISH_LANG => $englishText];
                $this->updateTranslateKey($translatesToImport[$bundle][$keyTranslation], $newTranslates);
            }
        }
        return $translatesToImport;
    }

    /**
     * Zwraca filtr bundle z którego mają zostać pobrane tłumaczenia
     */
    private function getBundleFilter()
    {
        $searchBundle = "";
        $sourceBundle = $this->sourceBundleName();
        if ($sourceBundle) {
            $searchBundle = $sourceBundle;
        }
        return $searchBundle;
    }

    /**
     * Zwraca ustawiona przez uzytkownika nazwe bundle z ktorego maja zostac pobrane tlumaczenia
     * @return bool|mixed
     */
    private function sourceBundleName()
    {
        if (!empty($this->translateSettings)) {
            return isset($this->translateSettings->settings[2]) ? $this->translateSettings->settings[2] : null;
        }
        return null;
    }

    /**
     * Aktualizuje klucz tlumaczenia
     * @param array $translateKey
     * @param array $newTranslations => array('language' => 'translate') - ex. ('pl' => '12345', 'en' => '54321')
     * @throws TranslToolException
     */
    private function updateTranslateKey(&$translateKey, $newTranslations)
    {
        if (empty($newTranslations)) {
            throw new TranslToolException("Nowe tłumaczenia nie mogą być puste");
        } else {
            //Ustawiam tablice jezykow dla ktorych maja zostac zaimportowane nowe tlumaczenia
            $filterLanguage = $this->getFilterLanguage();
            foreach ($newTranslations as $language => $text) {
                if (in_array($language, $filterLanguage)) {
                    $translateKey[$language] = $text;
                }
            }
        }
    }

    private function getFilterLanguage()
    {
        return $this->translateSettings->getFilterLanguage() && !in_array('all', $this->translateSettings->getFilterLanguage()) ? $this->translateSettings->getFilterLanguage() : \Kei\CustomerPanelBundle\Tools\TranslationTool\TranslationToolCommon::$availableLanguages;
    }
}
