<?php
/***
 * Obslugiwana jest przez klase TranslationToolCommand
 * Klasa generuje export  kluczy do pliku xls
 */

namespace Kei\CustomerPanelBundle\Tools\TranslationTool\Action;

use Kei\CustomerPanelBundle\Tools\TranslationTool\TranslationToolCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Liuggio\ExcelBundle\Factory;
use Kei\CustomerPanelBundle\Tools\TranslationTool\Exceptions\TranslToolException;

/**
 * Class KeyExport
 * @package Kei\CustomerPanelBundle\Tools\TranslationTool\Action
 * @SuppressWarnings
 */
class KeyExport extends TranslationToolCommon
{
    private $translates = array();
    const COMMAND_NEWKEYS = "newkeys";
    const COMMAND_CHANGEDKEYS = "changedkeys";
    const COMMAND_INTEGRITY = "integrity_test";
    private static $canUseKeyType = array(self::COMMAND_CHANGEDKEYS, self::COMMAND_NEWKEYS, self::COMMAND_INTEGRITY);
    /**
     * Generuje do pliku xls nowe(także, te której wartości są puste) lub zmienione klucze
     * @return string
     * @throws TranslToolException
     */
    public function run()
    {
        $this->checkCommandIsPossible(self::$canUseKeyType, $this->extraBehSettings);
        $this->translates = $this->getBundlesTranslate();
        if (empty($this->translates)) {
            throw new TranslToolException("Wystąpił problem z pobraniem Bundle'ów. Sprawdź w parametrze: translate_tool.sourcesTranslate, czy podałeś poprawną ścieżkę");
        }
        return $this->extraBehSettings !== self::COMMAND_INTEGRITY ? $this->exportTranslates() : $this->integrityTest();
    }

    /**
     * Generuje plik xls z dostarczonych kluczy do ustawionego folderu
     * @return string
     * @throws TranslToolException
     * @throws \PHPExcel_Exception
     */
    private function generateExcelFile()
    {
        if (empty($this->newTranslations)) {
            throw new TranslToolException("Niestety nie posiadamy kluczy do wygenerowania.");
        }

        $excelFactory = new Factory();
        $phpExcelObject = $excelFactory->createPHPExcelObject();
        $phpExcelObject->getProperties()->setCreator("liuggio")
            ->setLastModifiedBy("Kei.pl")
            ->setTitle("Kei.pl")
            ->setSubject("Kei.pl")
            ->setDescription("Klucze do tłumaczeń.")
            ->setKeywords("office 2005 openxml php")
            ->setCategory("Kei.pl-tłumaczenia");
        $phpExcelValue= $phpExcelObject->setActiveSheetIndex(0);
        $indexCell = 1;
        foreach ($this->newTranslations as $bundlePath => $bundleData) {
            foreach ($bundleData as $keyTranslate => $valueTranslate) {
                // Usuwamy absolutna sciezke, aby uzytkownik importujacy pozniej plik z poprawionymi tlumaczeniami mogl to zrobic z innego komputera
                $pathTranslateFile = str_replace($this->getParamFromConfig('translate_tool.kernel_dir'), "", $bundlePath);
                $phpExcelValue->setCellValue('A'.$indexCell, $pathTranslateFile);
                $phpExcelValue->setCellValue('B'.$indexCell, $keyTranslate);
                $phpExcelValue->setCellValue('C'.$indexCell, isset($valueTranslate['pl']) ? $valueTranslate['pl'] : ' ');
                $phpExcelValue->setCellValue('D'.$indexCell, isset($valueTranslate['en']) ? $valueTranslate['en'] : ' ');
                $indexCell++;
            }
        }
        $phpExcelObject->getActiveSheet()->setTitle('Kei.pl');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);
        // create the writer
        $writer = $excelFactory->createWriter($phpExcelObject, 'Excel5');
        // The save method is documented in the official PHPExcel library
        $fileName = $this->getPathToExlFile().'/'.'keysExcel_'.date('d_m_Y_h_i_s').'.xls';
        $writer->save($fileName);
        $this->checkFileIsExists($fileName);
        $this->newTranslations = [];
        return "Zapis pliku xls do $fileName powiódł się pomyślnie.";
    }
    /**
     * Zwraca zmienione tłumaczenia.
     * @param string $pathOldTranslates
     * @param string $pathNewTranslates
     */
    private function getChangedTransl($pathOldTranslates, $pathNewTranslates)
    {
        $generatedJson = $this->getNewAndOldIndex($pathOldTranslates, $pathNewTranslates);
        $oldIndex = $generatedJson[0];
        $newIndex = $generatedJson[1];
        $pathYmlFile = pathinfo($pathOldTranslates)['dirname'];
        foreach ($newIndex as $keyIndex => $keyValue) {
            if (isset($keyValue['pl']) && isset($oldIndex[$keyIndex]['pl'])) {
                if (htmlspecialchars_decode(stripslashes($newIndex[$keyIndex]['pl'])) !== htmlspecialchars_decode(stripslashes($oldIndex[$keyIndex]['pl']))) {
                    $this->newTranslations[$pathYmlFile][$keyIndex] = $newIndex[$keyIndex];
                }
            }
        }
    }

    /**
     * Funkcja zwraca dane z nowego i starego jsona
     * @param string $pathOldTranslates
     * @param string $pathNewTranslates
     * @return array
     * @throws TranslToolException
     */
    private function getNewAndOldIndex($pathOldTranslates, $pathNewTranslates)
    {
        $oldIndex = json_encode([]);
        $newIndex = json_encode([]);
        if (file_exists($pathOldTranslates)) {
            $oldIndex = @file_get_contents($pathOldTranslates);
        }
        if (file_exists($pathNewTranslates)) {
            $newIndex = @file_get_contents($pathNewTranslates);
        }
        $oldIndex = json_decode($oldIndex, true);
        $newIndex = json_decode($newIndex, true);
        $this->hasJsonDecodeError();
        return array($oldIndex, $newIndex);
    }
    /**
     * Zwraca nowe tłumaczenia, ktore zostaly dodane.
     * @param string $pathOldTranslates
     * @param string $pathNewTranslates
     */
    private function getNewTranslates($pathOldTranslates, $pathNewTranslates)
    {
        $generatedJson = $this->getNewAndOldIndex($pathOldTranslates, $pathNewTranslates);
        $oldIndex = $generatedJson[0];
        $newIndex = $generatedJson[1];
        $pathYmlFile = pathinfo($pathOldTranslates)['dirname'];
        foreach ($newIndex as $keyValue) {
            $keyIndex =  array_search($keyValue, $newIndex);
            if ((!isset($oldIndex[$keyIndex]['pl']) && isset($newIndex[$keyIndex]["pl"])) || !isset($oldIndex[$keyIndex]["en"])) {
                $this->newTranslations[$pathYmlFile][$keyIndex] = $newIndex[$keyIndex];
            }
        }
    }
    /**
     * sprawdza,czy mozna wykonać polecenie
     * @param array $arrayToCheck
     * @param string $valueToCheck
     * @throws TranslToolException
     */
    protected function checkCommandIsPossible($arrayToCheck, $valueToCheck)
    {
        if (!in_array($valueToCheck, $arrayToCheck)) {
            throw new TranslToolException("Nie można wykonać polecenia: $this->extraBehSettings. Spróbuj: changedkeys lub newkeys");
        }
    }
    /**
     * Funkcja generuje odpowiednia tablice z yml do zapisu do json/xls
     * @param array $yamlParser - tablica z której pobierane są zmienne
     * @param $language - język pliku yml pobrany z nazwy pliku,
     *             ktory automatycznie jest kluczem glownego klucza('$filePath;sciezka_do_ostatniego_klucza')
     * @param $filePath - nazwa pliku bez jezyka i rozszerzen
     */
    private function parseYml($yamlParser, $language, $filePath)
    {
        if ($yamlParser) {
            foreach ($yamlParser as $key => $value) {
                if (is_array($value)) {
                    $this->generateArray($key, $value, $language, $filePath);
                } else {
                    $this->keysTranslation[$filePath . $key][$language] = htmlspecialchars_decode($value);
                }
            }
        }
    }

    /**
     * Funkcja pomocnicza do generowania tablicy z yml
     * @param string $stringKey -  klucz do ktory dopelniana jest konkatenacja jesli ostatni klucz posiada tablice
     * @param array $parseArray - tablica z ktorej tworzona jest konkatencja
     * @param  string $lng - jezyk ktory jest kluczem ostatniego klucza tablicy $parseArray
     * @param string $filePath - nazwa pliku z ktorego pobrano yml
     */
    private function generateArray($stringKey, $parseArray, $lng, $filePath)
    {
        foreach ($parseArray as $keyArray => $valueArray) {
            if (is_array($valueArray)) {
                $this->generateArray($stringKey.'/'.$keyArray, $valueArray, $lng, $filePath);
            } else {
                $this->keysTranslation[$filePath . $stringKey . '/' . $keyArray][$lng] = $valueArray;
            }
        }
    }

    /**
     * Wykonuje export nowych, badz zmienionych kluczy
     * @throws TranslToolException
     */
    private function exportTranslates()
    {
        $this->getTranslByTypeKeys();
        return $this->generateExcelFile();
    }

    /**
     * Parsuje tablice z plikami yml do pliku _translation.new_index.json
     * @param array $ymlFiles - [main=> array("pl"=>"/afsasfjh/main.pl.yml", "en"=> "/afsasfjh/main.en.yml")]
     * @param string $pathFile - sciezka do zapisu pliku w/w
     */
    private function generateIndexFiles($ymlFiles, $pathFile)
    {
        if (empty($ymlFiles)) {
            return;
        }
        // Przechodzi po tablicy ze sciezkami do plikow Yml
        foreach ($ymlFiles as $fileName => $filePaths) {
            foreach ($filePaths as $lang => $path) {
                $yml = Yaml::parse($path, false, true);
                $baseNameFile = $fileName.";";
                // Tworzy odpowiedni format pliku json ze sparsowanego pliku yml
                $this->parseYml($yml, $lang, $baseNameFile);
            }
        }
        $this->generateJSON($this->keysTranslation, $pathFile);
    }

    /**
     * Zwraca sciezke do folderu w ktorym ma byc zapisany xls
     */
    private function getPathToExlFile()
    {
        $savePath = $this->translateSettings->getTargetPath();
        if (!$savePath) {
            $savePath = $this->getParamFromConfig("translate_tool.xls_dir");
            @mkdir($savePath, 0777, true);
        }
        $this->checkFileIsExists($savePath);
        return $savePath;
    }

    /**
     * Zwraca tlumaczenia  do exportu w zaleznosci od opcji do exportu: integrity_test/changedkeys/newkeys
     * @throws TranslToolException
     */
    private function getTranslByTypeKeys()
    {
        foreach ($this->translates as $path) {
            $this->keysTranslation = [];
            // Sprawdzamy, czy sciezka nie prowadzi do vendora kei, poniewaz wtedy sciezka do tlumaczen jest inna.
            if (preg_match("#".parent::VENDOR_DIR."#", $path)) {
                if ($vendorTranslate = $this->handleDirectoryVendor($path, false)) {
                    $path = reset($vendorTranslate);
                }
            }

            $translationPath = $path.''.$this->getParamFromConfig('translate_tool.fillSourceTranslate');
            $ymlFiles = $this->handleDirectory($translationPath, true, 'yml');
            $newIndexJsonPath = $translationPath.'/'.$this->getParamFromConfig('translate_tool.new_json_file');
            $oldIndexJsonPath = $translationPath.'/'.$this->getParamFromConfig('translate_tool.json_file');
            $this->generateIndexFiles($ymlFiles, $newIndexJsonPath);

            if ($this->extraBehSettings === self::COMMAND_CHANGEDKEYS) {
                $this->getChangedTransl($oldIndexJsonPath, $newIndexJsonPath);
            } else if ($this->extraBehSettings === self::COMMAND_NEWKEYS) {
                $this->getNewTranslates($oldIndexJsonPath, $newIndexJsonPath);
            } else if ($this->extraBehSettings === self::COMMAND_INTEGRITY) {
                $this->getChangedTransl($oldIndexJsonPath, $newIndexJsonPath);
                $this->getNewTranslates($oldIndexJsonPath, $newIndexJsonPath);
            }

        }
    }

    /**
     * Test integralnosci tlumaczen. Sprawdza, czy w tlumaczeniach zostaly wprowadzone zmiany.
     * @return string
     */
    private function integrityTest()
    {
        $this->getTranslByTypeKeys();
        if (empty($this->newTranslations)) {
            return "Test integralności przebiegł pomyślnie. Tłumaczenia nie posiadają żadnych zmian.";
        }
        echo "Brakujące tłumaczenia: \n";
        $this->writeMissTran();
        echo "Tłumaczenia posiadają zmiany. WYkonaj opcję eksportu zmienionych lub nowych kluczy.";
        return exit(160);
    }

    /**
     * Wyswietla brakujace tlumaczenia
     */
    private function writeMissTran()
    {
        $count = 0;
        foreach ($this->newTranslations as $translationsDir => $translationsValues) {
            foreach ($translationsValues as $translationsKey => $translationsValue) {
                $count ++;
                echo "Folder: $translationsDir\n";
                echo "Klucz: $translationsKey.\n";
                echo "____________________________________________________\n";
            }
        }
        echo "Ilość nowych lub zmienionych kluczy: $count.\n";
    }
}
