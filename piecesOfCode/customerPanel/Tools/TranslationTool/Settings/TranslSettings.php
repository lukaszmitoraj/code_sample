<?php

/**
 * Created by PhpStorm.
 * User: lukasz
 * Date: 18.01.17
 * Time: 10:11
 */
namespace Kei\CustomerPanelBundle\Tools\TranslationTool\Settings;

class TranslSettings
{
    private static $indexTargetPath = array("import" => 3, "export" => 1, "generate_yml" => 2);
    private static $indexFilterLanguage = array("import" => 1, "export" => 2, "generate_yml" => 1);
    public $settings = [];
    private $actionType;

    /**
     * TranslSettings constructor.
     * @param string $settings
     * @param string $actionType - typ akcji wykonanej na tlumaczeniach
     */
    public function __construct($settings, $actionType)
    {
        $this->settings = $this->getSettings($settings);
        $this->actionType = $actionType;
    }
    /**
     * Zwraca ustawienia dla opcji komendy ex. translate:generate generate_yml KeiCustomerBundle:sciezka/do/zapisu => array(KeiCustomerPanelBundle, scezka/do/zapisu);
     * @param $settings
     * @return mixed
     */
    protected function getSettings($settings)
    {
        return explode(":", $settings);
    }

    /**
     * Zwracam ustawienia, jako string
     * @return mixed
     */
    public function getSettingString()
    {
        return implode(':', $this->settings);
    }
    /**
     * Zwraca ustawiona przez użytkownika ścieżkę źródłowa
     * @return null|string
     */
    public function getSourcePath()
    {
        if (!empty($this->settings)) {
            return isset($this->settings[0]) ? $this->settings[0] : null;
        }
        return null;
    }

    /**
     * Zwraca ustawiony przez użytkownika jezyk dla ktorego ma zostac wykonana akcja
     * @return mixed|null
     */
    public function getFilterLanguage()
    {

        $filterLanguages = \Kei\CustomerPanelBundle\Tools\TranslationTool\TranslationToolCommon::$languageDefaultFilter;
        if (!empty($this->settings)) {
            // Ustawiam index w tablicy dla docelowej sciezki, poniewaz kazda akcja tlumaczen ma inna kolejnosc ustawien.
            $indexLanguage = self::$indexFilterLanguage[$this->actionType];
            $filterLanguages = isset($this->settings[$indexLanguage]) ? array($this->settings[$indexLanguage]) : $filterLanguages;
        }
        return $filterLanguages;
    }

    /**
     * Zwraca ustawiona przez użytkownika ścieżkę docelową
     * @return mixed|null
     */
    public function getTargetPath()
    {
        if (!empty($this->settings)) {
            // Ustawiam index w tablicy dla docelowej sciezki, poniewaz kazda akcja tlumaczen ma inna kolejnosc ustawien.
            $indexTargetPath = self::$indexTargetPath[$this->actionType];
            return isset($this->settings[$indexTargetPath]) ? $this->settings[$indexTargetPath] : null;
        }
        return null;
    }
}
