const MESSAGE_IMPLEMENT = 'You have to implement it';
export default class RedirectInterface {
  static isValid() {
    throw new Error(MESSAGE_IMPLEMENT);
  }

  static getUrl() {
    throw new Error(MESSAGE_IMPLEMENT);
  }

  static handle() {
    throw new Error(MESSAGE_IMPLEMENT);
  }

  static redirect() {
    throw new Error(MESSAGE_IMPLEMENT);
  }
}
