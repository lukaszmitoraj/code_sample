import { slugify } from 'services/String/StringHelper';
import RedirectInterface from '../RedirectInterface';
import RedirectException from '../RedirectException';

export default class ArticleRedirect extends RedirectInterface {
  static handle({ res, query, detail }) {
    if (!Object.keys(detail).length) {
      throw new RedirectException('Detail have to be not empty');
    }

    if (!ArticleRedirect.isValid(query, detail)) {
      const url = ArticleRedirect.getUrl(detail);
      ArticleRedirect.redirect(res, url);
    }
  }

  static isValid(query, detail) {
    return query.articleTitleSlug === slugify(detail.title);
  }

  static redirect(res, url) {
    if (res) {
      res.writeHead(301, {
        Location: url,
      });
      res.end();
    }
  }

  static getUrl(detail) {
    const { categoryTeaser: { name: categoryName, id: categoryId }, title, id } = detail;
    const slugTitleCategory = slugify(categoryName);
    const slugTitle = slugify(title);
    return `/${slugTitleCategory},${categoryId}/${slugTitle},${id}.html`;
  }
}
