import { slugify } from 'services/String/StringHelper';
import RedirectException from '../RedirectException';
import RedirectInterface from '../RedirectInterface';

export default class CategoryRedirect extends RedirectInterface {
  static handle({ res, query, category }) {
    if (!Object.keys(category).length) {
      throw new RedirectException('Detail have to be not empty');
    }
    if (!CategoryRedirect.isValid(query, category)) {
      const url = CategoryRedirect.getUrl(category);
      CategoryRedirect.redirect(res, url);
    }
  }

  static isValid(query, detail) {
    return query.name === slugify(detail.name);
  }

  static redirect(res, url) {
    if (res) {
      res.writeHead(301, {
        Location: url,
      });
      res.end();
    }
  }

  static getUrl(detail) {
    const { name, id } = detail;
    const slugTitleCategory = slugify(name);
    return `/${slugTitleCategory},${id}.html`;
  }
}
