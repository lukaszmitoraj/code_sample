import ArticleRedirect from './Modes/ArticleRedirect';
import CategoryRedirect from './Modes/CategoryRedirect';
import RedirectException from './RedirectException';

export default class Redirect {
  static handleArticle(res, query, detail) {
    try {
      ArticleRedirect.handle({ res, query, detail });
      return true;
    } catch (e) {
      if (e instanceof RedirectException) {
        return false;
      }
      throw new Error(e);
    }
  }

  static handleCategory(res, query, category) {
    try {
      CategoryRedirect.handle({ res, query, category });
      return true;
    } catch (e) {
      if (e instanceof RedirectException) {
        return false;
      }
      throw new Error(e);
    }
  }
}
