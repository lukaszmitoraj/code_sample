export default class RedirectException extends Error {
  constructor(...args) {
    super(...args);
    RedirectException.captureStackTrace(this, RedirectException);
  }
}
